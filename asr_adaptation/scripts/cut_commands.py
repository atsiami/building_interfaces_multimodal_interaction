import sys
import os
import rosbag
from std_msgs.msg import Int32, String

wav_file=sys.argv[1]
srt_file=sys.argv[2]
offset=float(sys.argv[3])
fs=16000
boundary_offset=0.4

audio_topic='audiorecord'

# read annotation times in sec 
start_times=[]
end_times=[]
labels=[]
for ln in open(srt_file,'r'):
  ln=ln.rstrip('\n')
  tmp=ln.split(" ")
  start_times.append(float(tmp[0])/1000-boundary_offset)
  end_times.append(float(tmp[1])/1000-boundary_offset)
  labels.append(tmp[2])

#print start_times[0], end_times[0], offset

for i in range(0,len(labels)):
  clip_name = os.path.splitext(wav_file)[0] + '_' + labels[i] + '_%d' % (i+1) + '.wav'
  os.system('sox %s -r %d %s trim %f %f' % (wav_file,fs,clip_name,start_times[i]-offset,end_times[i]-start_times[i]))
  #print 'sox %s %s trim %f %f' % (wav_file,clip_name,start_times[i]-offset,end_times[i]-start_times[i])
  print "%s %f %f" % (os.path.splitext(os.path.basename(clip_name))[0],start_times[i]-offset,end_times[i]-offset)
