% Read data from raw data file (audio)

% Input:
% -----
% file_name: the .raw file name.
% num_channels: number of channels interleaved in the raw file.
% a: forgetting factor for recursive Welch periodogram (smoothing of CS for
%    successive frames).
% precision: datatype of raw samples (e.g. 'int16' for 16-bit PCM).
% machine_format: endianness. 'l' for little, 'b' for big.
%
% Output:
% ------
% X: audio signal(s)
% X = raw_read('out.raw',8,'int16','l')


function X = raw_read(file_name, num_channels, precision, machine_format)

[path, name, extension] = fileparts(file_name);
if isempty(extension)
    extension = '.raw';
end
file_name = fullfile(path, [name, extension]);

file_id = fopen(file_name);
if file_id < 0
    error(['Unable to open file ', file_name]);
end
close_file_cleanup_variable = onCleanup(@() fclose(file_id));

[X_raw, count] = fread(file_id, [num_channels, Inf], precision, 0, machine_format);
num_samples = count / num_channels;
if num_samples ~= floor(num_samples)
    warning('raw_read:ChannelsSampleNumberMismatch', 'Number of samples is not equal for all channels');
end

X = shiftdim(X_raw, 1);

end