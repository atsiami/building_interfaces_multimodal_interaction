#!/usr/bin/env python

import rospy
from std_msgs.msg import String, Bool, UInt8
import roslib
#roslib.load_manifest('store_stereo_image')
import sys 
import cv2
import sensor_msgs
from cv_bridge import CvBridge, CvBridgeError
import time
import numpy as np

class ImageReader:
  def __init__(self):

    rospy.init_node("press_to_gesture", anonymous=True)
    ## rosbags
    self.sub_topic = rospy.get_param('/kinect_topic', '/camera/rgb/image_color/')
    self.out_topic = rospy.get_param('/activity_det_topic', '/gesture_segmentation')
    print self.sub_topic, self.out_topic
    self.rgb_sub = rospy.Subscriber(self.sub_topic, sensor_msgs.msg.Image, self.rgb_callback)
    #self.depth_sub = rospy.Subscriber("/camera/depth/image/", sensor_msgs.msg.Image, self.depth_callback)
    self.key_publisher = rospy.Publisher(self.out_topic, String)
    self.bridge = CvBridge()
    self.frameCounter_rgb = 0
    rospy.on_shutdown(self.shutdown)

    cv2.namedWindow("Kinect - RGB", 1)
    #self.frameCounter_depth = 0
    #cv2.namedWindow("Kinect - depth", 1)


  def rgb_callback(self,imageMessage):
    self.frameCounter_rgb = self.frameCounter_rgb + 1
    #rospy.loginfo(rospy.get_caller_id() + "rgb callback was triggered (frame %d)" % self.frameCounter_rgb)
    try:
      cv_image = self.bridge.imgmsg_to_cv2(imageMessage, desired_encoding="bgr8")
      cv2.imshow("Kinect - RGB", cv_image)
      key = cv2.waitKey(1)
      timestamp = imageMessage.header.stamp.to_nsec()
      if key!=-1:
        print key
      if (key==1048689) or (key==113): # 'q' or "Q"
        msg = "100 100"
        self.key_publisher.publish(msg)
        print "You pressed 'q'. Goodbye!"
        rospy.signal_shutdown("You pressed 'q'. Goodbye!")
      if (key==1048673) or (key==97): # a
          msg = str(0) + " "  + str(timestamp)
          #time.sleep(7)
          self.key_publisher.publish(msg)
          print "I just published '0' (START) from " + str(timestamp) + "!"
      if (key == 1048691) or (key==115):  # s
          msg = str(1) + " "  + str(timestamp)
          print "I just published '1' (STOP) from " + str(timestamp) + "!"
          self.key_publisher.publish(msg)
      if (key == 1048676) or (key==100):  # d
          msg = str(2) + " "  + str(timestamp)
          self.key_publisher.publish(msg)
          print "I just published '2' (REJECT) from " + str(timestamp) + "!"
		  
    except CvBridgeError, e:
      rospy.loginfo("Conversion failed")
      print("Conversion failed")

  def depth_callback(self,imageMessage):
    self.frameCounter_depth = self.frameCounter_depth + 1
    rospy.loginfo(rospy.get_caller_id() + "depth callback was triggered (frame %d)" % self.frameCounter_depth)
    try:
      cv_image = self.bridge.imgmsg_to_cv2(imageMessage, desired_encoding="16UC1")
      depth_array = np.array(cv_image, dtype=np.float32)
      cv2.normalize(depth_array, depth_array, 0, 1, cv2.NORM_MINMAX)
      cv2.imshow("Kinect - depth", depth_array)
      cv2.waitKey(3)
    except CvBridgeError, e:
      rospy.loginfo("Conversion failed")
      print("Conversion failed")


  def shutdown(self):
      # cv2.destroyAllWindows()
    rospy.loginfo("Shutting down node...")

def main(args):
  r = ImageReader()
  try:
    rospy.spin();
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()


if __name__ == '__main__':
  main(sys.argv)



