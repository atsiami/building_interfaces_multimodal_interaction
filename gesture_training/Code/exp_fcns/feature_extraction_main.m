function feature_extraction_main(params, db_params, config_file, bad_bags)

setenv('PYTHONPATH', params.paths.PYTHONPATH)
setenv('LD_LIBRARY_PATH', params.paths.LD_LIBRARY_PATH)
rosbags = db_params.rosbags;
for b=1:length(rosbags)
    if exist('bad_bags', 'var') && ~ismember(rosbags(b).file, bad_bags)
        continue;
    end
    [~,filename] = fileparts(rosbags(b).file);
    metadata_file = fullfile(params.paths.metadata, [filename '.txt']);
    fid = fopen(metadata_file, 'w');
    
    for i=1:length(rosbags(b).labels)
        fprintf(fid, '%s %s %s %s\n', rosbags(b).start_times{i},...
            rosbags(b).end_times{i}, rosbags(b).labels{i}, rosbags(b).codenames{i});
    end
    
    fclose(fid);
    
    fprintf('Extracting features from rosbag %d/%d (%s)\n', b, length(rosbags), rosbags(b).file);
    if strcmp(params.dataset.channel, 'rgb')
        command = sprintf('/usr/bin/python extract_features_rosbag.py %s %s %s -o %s --visualize 0', ...
            config_file, rosbags(b).file, metadata_file, params.paths.features);
    else
        command = sprintf('/usr/bin/python extract_features_depth_rosbag.py %s %s %s -o %s --visualize 0', ...
            config_file, rosbags(b).file, metadata_file, params.paths.features);
    end
    
    if params.dataset.decimation_factor > 1
        command = [command ' --downsample ' num2str(params.dataset.decimation_factor)];
    end
    if params.dataset.frame_stride > 1
        command = [command ' --frame_stride ' num2str(params.dataset.frame_stride)];
    end    
    
    attempts = 0;
    while 1
        attempts = attempts + 1;
        status = system(command);
        if status % there was an error
            fprintf('Error extracting features...\n');
            if attempts < 20
                fprintf('Re-attempting...\n');
            else
                fprintf('Error extracting features. Losing hope...\n');
                break
            end 
        else % everything is ok
            break
        end
    end

end