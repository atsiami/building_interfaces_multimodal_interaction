function f = load_features(clip, params)

    if (params.reuse.features) && exist(fullfile(params.paths.features, sprintf('%s.mat', clip.name)), 'file')
        try
            f = parload(fullfile(params.paths.features, clip.name));
        catch
            params.reuse.features = false;
            f = load_features(clip, params);
        end
    else
        f = extractFeatures(clip, params.features);
        parsave(fullfile(params.paths.features, clip.name), f);
    end
    
    d = params.features.descriptors;
    if ~isempty(find(isnan(f.(d{1})), 1))
        params.reuse.features = false;
        f = load_features(clip, params);
    end
end