function db_params = configDataset_BM(params)
	%% database configuration
    %params = ReadYaml(config_file);
%     actions_full = {'AvoidObstacle','ComeCloser','ComeHere','GoAway','GoStraight','GoThroughDoor','Help','LetsGo','PerformTask','Stop','TurnLeft','TurnRight','WantSitDown','WantStandUp','WhereAmI','Yes','No','Park', 'WhatTime', 'StandUp', 'SitDown'};
    actions_full = params.vocabulary_full;
    rng(0);
    subject_names = params.subjects;
    subject_ids = 1:length(subject_names);
	actions = params.vocabulary;
    if isfield(params, 'background_model') && params.background_model
        bm = true;
        actions{end+1} = 'bm';
        background_actions = setdiff(actions_full, actions);
        if isfield(params, 'exclude_bm')
            background_actions = setdiff(background_actions, params.exclude_bm);
        end
    else
        bm = false;
    end
	rosbags_list = params.files_list;
	
	temp = textscan(fopen(rosbags_list), '%s %s %s');
    rosbags = arrayfun(@(w,x,y) struct('subject_name', w, 'file', x, 'annotation_file', y), temp{1}, temp{2}, temp{3});
% 	rosbags.subjectName = temp{1};
% 	rosbags.file = temp{2};
%     rosbags.annot_file = temp{3};
	num.classes = length(actions);
	num.subjects = length(subject_names);
	num.videos = length(rosbags);

	% collect all action instances
	clips = struct('subject_id', -1, 'subject_name', '', 'rosbag', '', 'class_label', '', 'iteration', -1);
	i = 0;
    iterCounter = zeros(num.subjects, length(actions));
    iterCounter_full = zeros(num.subjects, length(actions_full));
    for v=1:num.videos
		if ~ismember(rosbags(v).subject_name, subject_names)
			continue;
		end
		rosbag= rosbags(v).file;
        [~,rosbag_basename] = fileparts(rosbag);
		subject = subject_ids(strcmp(rosbags(v).subject_name, subject_names));
		fid = fopen(fullfile(params.extracted_frames_path, rosbag_basename, [rosbag_basename '.txt']));
		annotation = textscan(fid, '%s %s %s');
        fclose(fid);
        
        if isfield(params, 'clip_limit')
            if params.clip_limit < length(annotation{3})
                rand_ind = randperm(length(annotation{3}), params.clip_limit);
                annotation = cellfun(@(x) x(sort(rand_ind(1:params.clip_limit))), annotation,  'UniformOutput', false);
            end
        end
        
        rosbags(v).codenames = cell(1);
        rosbags(v).start_times = cell(1);
        rosbags(v).end_times = cell(1);
        rosbags(v).labels = cell(1);
        rosbags(v).settings = cell(1);
        segment_counter = 1;
        
        for iSeg=1:length(annotation{3})
			iAction = find(strcmp(actions, annotation{3}{iSeg}));
%             annotation{3}{iSeg}
            if isempty(iAction)
                if bm && any(strcmp(background_actions, annotation{3}{iSeg}))
                    iAction = find(strcmpi(actions, 'bm'));
                    iAction_real = find(strcmp(actions_full, annotation{3}{iSeg}));
                    iterCounter_full(subject, iAction_real) = iterCounter_full(subject, iAction_real) + 1 ;
                else
                    continue;
                end                
            end
            
			iterCounter(subject, iAction) = iterCounter(subject, iAction) + 1 ;
			i = i + 1;
			clips(i).subject_id = subject;
			clips(i).subject_name = subject_names{subject};
			clips(i).rosbag = rosbag;
            clips(i).frames = fullfile(params.extracted_frames_path, rosbag_basename, 'image%06d.png');
            clips(i).startFrame = str2double(annotation{1}{iSeg});
            clips(i).endFrame = str2double(annotation{2}{iSeg});
            
            
            if bm && strcmpi(actions{iAction}, 'bm')
                clips(i).class_label = 'bm';
                clips(i).iteration = iterCounter_full(subject, iAction_real);
            else
                clips(i).class_label = annotation{3}{iSeg};
                clips(i).iteration = iterCounter(subject, iAction);
            end
            clips(i).class = iAction;
			clips(i).name = sprintf('%s_%s_%d', clips(i).subject_name,...
				annotation{3}{iSeg}, clips(i).iteration);
            
            rosbags(v).codenames{segment_counter} = clips(i).name;
            rosbags(v).start_times{segment_counter} = annotation{1}{iSeg};
            rosbags(v).end_times{segment_counter} = annotation{2}{iSeg};
            rosbags(v).labels{segment_counter} = annotation{3}{iSeg};
            
            segment_counter = segment_counter + 1;
        end
    end
    
    if isfield(params, 'training') && ...
            isfield(params, 'testing')
        testing_subject_names = params.testing;
        training_subject_names = params.training;
        testing_subject_ids = -1*ones(length(testing_subject_names), 1);
        training_subject_ids = -1*ones(length(training_subject_names), 1);
        for i=1:length(testing_subject_names)
            testing_subject_ids(i) = subject_ids(strcmp(testing_subject_names{i}, subject_names));
        end
        for i=1:length(training_subject_names)
            training_subject_ids(i) = subject_ids(strcmp(training_subject_names{i}, subject_names));
        end
    else
        testing_subject_ids = subject_ids;
        training_subject_ids = subject_ids;
    end


    splits(1:length(testing_subject_ids)) = struct('training', -1, 'testing', -1, 'test_subject', '-1', 'name', '-1');
    for i=1:length(testing_subject_ids)
        splits(i).testing = find([clips.subject_id] == testing_subject_ids(i));
        splits(i).training = find( ([clips.subject_id] ~= testing_subject_ids(i))' &...
            sum(cell2mat(arrayfun(@(x) [clips.subject_id]'==x, training_subject_ids, 'UniformOutput', false)), 2));
        splits(i).test_subject = testing_subject_ids(i);
        splits(i).name = sprintf('test_%s', subject_names{testing_subject_ids(i)});
        splits(i).msg = sprintf('Leave-one-out test subject: %s', subject_names{testing_subject_ids(i)});
    end
    
    
	db_params.clips = clips;
	db_params.num = num;
	db_params.annotation = [db_params.clips.class]';
	db_params.classes = 1:num.classes;
	db_params.class_labels = actions;
	db_params.subjects = subject_ids;
	db_params.subject_names = subject_names;
    db_params.splits = splits;
    db_params.rosbags = rosbags;

    

end
