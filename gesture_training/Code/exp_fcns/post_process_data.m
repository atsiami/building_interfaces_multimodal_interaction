function [] = post_process_data(params)

setenv('PYTHONPATH', params.paths.PYTHONPATH)
setenv('LD_LIBRARY_PATH', params.paths.LD_LIBRARY_PATH)

f = fopen(params.dataset.files_list);
temp = textscan(f, '%s %s %s');
fclose(f);
rosbags = arrayfun(@(w,x,y) struct('subject_name', w, 'file', x, 'annotation_file', y), temp{1}, temp{2}, temp{3});

decim = params.data_post_process.decimation_factor;
stride = params.data_post_process.frame_stride;
output_path = fullfile(params.paths.features, 'extracted_frames');
if ~exist(output_path, 'dir')
    mkdir(output_path);
end

python_script = fullfile( fileparts(mfilename('fullpath')), 'post_process_data.py'); 
for i=1:length(rosbags)
    fprintf('Extracting frames from rosbag %s...\n', rosbags(i).file);
    command = ['python ', python_script, ' ', rosbags(i).file,  ' ', rosbags(i).annotation_file,...
        ' -v 0 ', '-s ' num2str(stride), ' -d ', num2str(decim) ' -o ', output_path ];
    status = system(command);
    if status
        error('Error extracting frames form rosbags');
    end
end
    