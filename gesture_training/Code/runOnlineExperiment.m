function [] = runOnlineExperiment(config_file)
    params = config_parser(config_file);

    if params.state < 0.5
        post_process_data(params);
    end
    
    db_params = configDataset_BM(params.dataset);
    
    if params.state < 1
        feature_extraction_wrapper(params, db_params);
    end


	params.paths.training_root = params.paths.training;
    params.paths.testing_root = params.paths.testing;
    params.paths.encodings_root = params.paths.encodings;
    splits = db_params.splits;
    
    params.reuse.features = 1;
    
	for s=1:length(splits)
		print_dashed(splits(s).msg);
		train_ind = splits(s).training;
		training_clips = db_params.clips(train_ind);
		params.paths.training = fullfile(params.paths.training_root, splits(s).name);
        params.paths.testing = fullfile(params.paths.testing_root, splits(s).name);
        params.paths.encodings = fullfile(params.paths.encodings_root, splits(s).name);
		checkPaths(params.paths.training, 'create');
        checkPaths(params.paths.testing, 'create');
        checkPaths(params.paths.encodings, 'create');

		%% Compute the Codebook
		if params.state <2
			compute_codebook_wrapper(params, training_clips, params.global_seed);
		end

		if params.state<3			
            parfor i=1:length(training_clips)
				% extract features
				msg_size = fprintf('Loading features: clip %d/%d', i, length(training_clips)); 
				f = load_features(training_clips(i), params);
				erase_msg(msg_size);
				
				% encode features
				msg_size = fprintf('Encoding features: clip %d/%d\n', i, length(training_clips)); 
				encoded_features = feature_encoding_wrapper(f, params, training_clips(i).name);
            end
		end

		%% Train SVMs
        if params.state < 4
		   svm_training_wrapper(params, training_clips, db_params.annotation(train_ind));
        end
        
        mat2c(params.paths.training);
        
        %% Testing
        if params.test
            test_ind = splits(s).testing;
            testing_clips = db_params.clips(test_ind);

            d = params.features.descriptors;
            if params.classification.combine
                d{end+1} = 'combined';
            end
            classes = init_struct_array(d, length(testing_clips));
            probs = init_struct_array(d, length(testing_clips));

            parfor i=1:length(testing_clips)
                msg_size = fprintf('Testing clip %d/%d\n', i, length(testing_clips)); 

                % extract features
                f = load_features(testing_clips(i), params);

                % encode features
                encoded_features = feature_encoding_wrapper(f, params, testing_clips(i).name);

                % classify
                [classes(i), probs(i)] = svm_testing_wrapper(encoded_features, params);
                if classes(i).(d{1}) < 0
                    for id=1:length(d)
                        classes(i).(d{id}) = round(rand(1,1)*db_params.num.classes)+1;
                    end
                end
            end
            accuracy = compute_accuracy(classes, db_params.annotation(test_ind));
            confusion_matrix = confMatrix_multiclass(classes, db_params.annotation(test_ind), db_params.num.classes);
            parsave(fullfile(params.paths.testing, 'results_long.mat'), accuracy, confusion_matrix, classes, probs);        
        end
	end    
end