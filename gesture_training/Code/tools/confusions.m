function [ cmat ] = confusions( config_file , codepath)
close all;
if ~exist('codepath', 'var')
    codepath = '../';
end
addpath(genpath(codepath));

params = config_parser(config_file);
db_params = configCVSP(params.dataset);
d = params.features.descriptors;
if params.classification.combine
    d{end+1} = 'combined';
end
cmat_all = init_struct_array(d, length(db_params.splits));
cmat = init_struct_array(d, 1);
confusions = init_struct_array(d, 1);
for id=1:length(d)
    confusions.(d{id}) = {};
end

for i=1:length(db_params.splits)
    try
        res = parload(fullfile(params.paths.testing, db_params.splits(i).name, 'results_long.mat'));
    catch
        fprintf('Results for %s do not exist\n', db_params.subject_names{i});
    end
    
    cmat_all(i) = res.confusion_matrix;
    for id=1:length(d)
        confusions.(d{id}) = [confusions.(d{id}); db_params.class_labels(db_params.annotation(db_params.splits(i).testing))',  db_params.class_labels([res.classes.(d{id})])'];
    end
end

% for i=1:length(d)
%     cmat.(d{i}) = zeros(db_params.num.classes);
%     for s=1:length(db_params.splits)
%         cmat_all(s).(d{i})(isnan(cmat_all(s).(d{i}))) = 0;
%         cmat.(d{i}) = cmat.(d{i}) + cmat_all(s).(d{i});
%     end
%     
%     f = figure(i);
%     cmat.(d{i}) = cmat.(d{i}) ./ repmat(sum(cmat.(d{i}), 2), 1, size(cmat.(d{i}), 2));
%     imagesc(cmat.(d{i}));
%     colorbar;
%     xticklabel_rotate(1:length(db_params.class_labels), 45, db_params.class_labels, 'FontSize', 30)
%     set(gca, 'YTick', 1:length(db_params.class_labels),'YTickLabel', db_params.class_labels)
%     set(gca,'FontSize', 30, 'FontName', 'Helvetica')
% %     set(gcf, 'color', 'none', 'inverthardcopy', 'off');
%     print(fullfile(params.paths.testing, ['conf_matrix_' d{i} '.png']), '-dpng');
% end


for id=1:length(d)
    ind = ~cellfun(@strcmp, confusions.(d{id})(:,1), confusions.(d{id})(:,2));
    confusions.(d{id}) = confusions.(d{id})(ind,:);
end
confusions.MBH

end

